//code
const express = require('express')
const app = express()
const port = process.env.PORT || 8080

app.get('/', (req, res) => {
  res.send('Hello World, I am Pavan')
})

app.listen(port, '0.0.0.0', () => {
  console.log(`Example app listening at http://'0.0.0.0':${port}`)
})